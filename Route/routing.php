<?php

function doRouting(): array
{
    load('toUpperCamelCase');

    $url = substr($_SERVER['REQUEST_URI'], 1);
    
    $rootDir = getenv(NGIN_ENV_VAR_PREFIX . 'ROOT') . DIRECTORY_SEPARATOR . 'Subject' . DIRECTORY_SEPARATOR . 'src' ;

    $subdir = '';
    $currentDir = $rootDir;

    $route = [];
    $params = [];

    $urlParts = explode('/', $url);

    while(($urlPart = array_shift($urlParts)) !== false)
    {
        if($urlPart == '')
            continue;

        $nextdir = toUpperCamelCase($urlPart);

        if(!file_exists($currentDir . DIRECTORY_SEPARATOR . $nextdir . DIRECTORY_SEPARATOR . '..'))
        {
            $action = $urlPart;
            break;
        }
        else
            $rootDir = $currentDir;

        $route[] = $urlPart;

        $currentDir .= DIRECTORY_SEPARATOR . $nextdir ;
    }

    $params = $urlParts;

    register([$subdir], $currentDir);

    return ['dir' => $currentDir.$subdir, 'action' => $action, 'params' => $params];
}