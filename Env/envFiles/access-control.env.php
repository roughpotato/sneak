<?php

return array(
    'ACCESS_CONTROL_ALLOW_ORIGIN' => '127.0.0.1, localhost',
    'ACCESS_CONTROL_MAX_AGE' => 86400
);