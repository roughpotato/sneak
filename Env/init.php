<?php

require_once __DIR__.'/../Service/directory.php';

const NGIN_ENV_VAR_PREFIX = 'SNKNGIN_';

$files = getFiles(__DIR__.'/envFiles', '/(.*)(\.env\.php)$/');

if(empty($files))
{
    echo "No env files found";
    exit;
}
else
{

    foreach($files as $file)
    {
        foreach(require __DIR__.'/envFiles/'.$file as $key => $value)
        {
            putenv(NGIN_ENV_VAR_PREFIX . $key.'='.$value);
        }
    }
}


