<?php

namespace Sneak\Data;

use SQLite3;

class DbConnectionHolder
{
    private static $dbConnection = null;

    public static function getConnection()
    {
        if(isset(self::$dbConnection))
            return self::$dbConnection;

        $dbname = getenv(NGIN_ENV_VAR_PREFIX . 'DB_NAME', true);

        self::$dbConnection = new SQLite3($dbname);

        return self::$dbConnection;
    }
}