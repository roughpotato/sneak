<?php

    use Sneak\Annotation\Method;

    #[Method('GET')]
    #[Route('/broker/{toto}/{tutu}')]
    function get($toto, $tutu)
    {
        load('fetchAll');
        echo $toto." ".$tutu;
        fetchAll('BROKER');
    }

    #[Method('POST')]
    #[Route('/broker')]
    function post()
    {
        echo 'post';
    }