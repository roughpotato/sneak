<?php

$subdirectories = array(
    'Sub'
);

register($subdirectories, __DIR__);


return array(
    'fetchAll' => __DIR__ . DIRECTORY_SEPARATOR . 'queries.php',
    'get' => __DIR__ . DIRECTORY_SEPARATOR . 'routes.php',
    'post' => __DIR__ . DIRECTORY_SEPARATOR . 'routes.php'
);