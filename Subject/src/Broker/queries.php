<?php

use Sneak\Data\DbConnectionHolder;

function fetchAll($table)
{
    $db = DbConnectionHolder::getConnection();

    $sql = 'SELECT id, name FROM ' . $table;

    $stmt = $db->prepare($sql);

    $result = $stmt->execute();

    $rows = [];

    while($row = $result->fetchArray(SQLITE3_ASSOC))
    {
        $rows[] = $row;
    }

    jsonResponse($rows);
}