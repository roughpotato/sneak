<?php

namespace Sneak\Annotation;

use Attribute;

#[Attribute(Attribute::TARGET_FUNCTION)]
class Route
{
    private string $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}