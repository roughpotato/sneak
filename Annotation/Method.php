<?php

namespace Sneak\Annotation;

use Attribute;

#[Attribute(Attribute::TARGET_FUNCTION)]
class Method
{
    private array $values;

    public function __construct(...$values)
    {
        $this->values = $values;
    }

    public function getValues(): array
    {
        return $this->values;
    }
}