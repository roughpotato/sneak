# Hi.

This is a test project of a kind of framework in functionnal (or at least non object oriented) programming.

User code would happen to rely in Subject sub dir.

An Apache (or any other web server) URL rewrite is required. An apache default conf follows.

I wrote these lines on fifth commit so this file is probably out of date at the time you read it.

--- 

`DocumentRoot /var/www/html/sneak`  
`<Directory /var/www/html/sneak>`  
&nbsp;&nbsp;&nbsp;&nbsp;`AllowOverride None`  
&nbsp;&nbsp;&nbsp;&nbsp;`Require all granted`  
&nbsp;&nbsp;&nbsp;&nbsp;`FallbackResource /index.php`  
`</Directory>`