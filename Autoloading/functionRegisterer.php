<?php

use Sneak\Autoloading\FunctionLoader;

function register(array $subdirectories, string $rootDir): void
{
    FunctionLoader::addToKnowledge($subdirectories, $rootDir);
}

function load(...$functions): void
{
    FunctionLoader::load(...$functions);
}

function isKnownFunction(string $function): bool
{
    return FunctionLoader::isKnownFunction($function);
}