<?php

namespace Sneak\Autoloading;

use Sneak\Exception\ConfigurationException;

class FunctionLoader
{
    private static array $fileByFunctionName = [];

    public static function addToKnowledge(array $subdirectories, string $rootDir): void
    {
        foreach($subdirectories as $dir)
        {
            if(is_string($dir) && !empty($dir))
            {
                $init = include $rootDir.'/'.$dir.'/init.php';

                if(!is_array($init))
                {
                    throw new ConfigurationException('The init file of the directory '.$rootDir.'/'.$dir.'/init.php must return an array.');
                }

                self::$fileByFunctionName = array_merge(self::$fileByFunctionName, $init);
            }
        }
    }

    public static function isKnownFunction(string $function): bool
    {
        return array_key_exists($function, self::$fileByFunctionName);
    }

    public static function isLoadedFunction(string $function): bool
    {
        return function_exists($function);
    }

    public static function load(...$functions): void
    {
        foreach($functions as $function)
        {
            if(!self::isKnownFunction($function))
            {
                throw new ConfigurationException('The function '.$function.' is not known.');
            }
            
            require_once self::$fileByFunctionName[$function];
        }
    }
}