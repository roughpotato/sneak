<?php

spl_autoload_register(function($fqcn)
{
    $filename = str_replace('\\', DIRECTORY_SEPARATOR, $fqcn);
    $filename = str_replace('Sneak', '', $filename);
    $filename .= '.php';

    $rootDir = getenv(NGIN_ENV_VAR_PREFIX . 'ROOT', true);

    $file = $rootDir . $filename;
    
    if(file_exists($file))
    {
        require_once $file;
    }
    else
    {
        $possibleAutoloadSubDirs = require __DIR__ . DIRECTORY_SEPARATOR . 'possibleAutoloadSubDirs.php';

        $dir = dirname($file);
        $basename = basename($file);

        foreach($possibleAutoloadSubDirs as $subDir)
        {
            $file = $dir . DIRECTORY_SEPARATOR . $subDir . DIRECTORY_SEPARATOR . $basename;
            
            if(file_exists($file))
            {
                require_once $file;
                return;
            }
        }
    }
});