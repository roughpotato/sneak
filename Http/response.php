<?php

function response($data = null, int $code = 200)
{
    header($_SERVER['SERVER_PROTOCOL'].' '.$code);

    if($data)
        echo $data;

    exit;
}

function jsonResponse($data, int $code = 200)
{
    header('Content-Type: application/json');

    response(json_encode($data), $code);
}