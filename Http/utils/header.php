<?php

use Sneak\Exception\GenericException;

function putHeaders(array $headers): void
{
    foreach($headers as $headerName => $headerValue)
    {
        if(!in_array($headerName, require __DIR__.'/allowedHeaders.php'))
            throw new GenericException('Invalid header name (case sensitive)');

        header($headerName.': '. (string) $headerValue);
    }
} 