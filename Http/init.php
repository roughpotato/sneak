<?php

$subdirectories = array(
    'utils'
);

register($subdirectories, __DIR__);

return array(
    'response' => __DIR__.'/response.php',
    'jsonResponse' => __DIR__.'/response.php',
    'acceptRequest' => __DIR__.'/request.php',
    'handleRequest' => __DIR__.'/request.php'
);