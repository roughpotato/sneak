<?php

use Sneak\Exception\MethodNotAllowedException;
use Sneak\Exception\RouteNotFoundException;
use Sneak\Annotation\Method;

load('doRouting', 'getFunctionAttributes', 'putHeaders');

function handleRequest(): void
{
    $route = doRouting();

    if(!isKnownFunction($route['action']))
    {
        throw new RouteNotFoundException($route);
    }

    load($route['action']);

    acceptRequest($route['action']);

    $route['action'](...$route['params']);
}

function acceptRequest(string $function): void
{
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS')
    {
        if(getenv(NGIN_ENV_VAR_PREFIX . 'ACCEPT_OPTIONS_REQUESTS', true) != true)
            throw new MethodNotAllowedException();

        $headers = ['Access-Control-Allow-Methods' => implode(', ', getFunctionAttributes($function, Method::class)),
                    'Access-Control-Allow-Headers' => 'Content-Type',
                    'Access-Control-Allow-Origin' => getenv(NGIN_ENV_VAR_PREFIX . 'ACCESS_CONTROL_ALLOW_ORIGIN', true),
                    'Access-Control-Max-Age' => getenv(NGIN_ENV_VAR_PREFIX . 'ACCESS_CONTROL_MAX_AGE', true)];

        putHeaders($headers);
    }
    else
    {
        if(!in_array($_SERVER['REQUEST_METHOD'], getFunctionAttributes($function, Method::class)))
            throw new MethodNotAllowedException();
    }
}