<?php

function getFunctionAttributes(string $function, string $attributeClass = ''): array
{
    $reflection = new ReflectionFunction($function);

    $reflectionAttributes = $reflection->getAttributes($attributeClass) ?? [];

    $attributes = array();

    foreach($reflectionAttributes as $reflectionAttribute)
    {
        $attributes = $reflectionAttribute->getArguments();
    }

    return $attributes;
}