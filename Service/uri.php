<?php

function isValidUriPart(string $uriPart): bool
{
    return preg_match('/^[a-zA-Z0-9]+$/', $uriPart);
}