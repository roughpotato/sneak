<?php

function getDirectories(string $dir): array
{
    $directories = array();

    foreach(scandir($dir) as $subdir)
    {
        if($subdir == '.' || $subdir == '..')
            continue;

        if(is_readable($subdir) && is_dir($subdir))
            $directories[] = $subdir;
    }

    return $directories;
}

function getFiles(string $dir, string $pattern): array
{
    $files = array();

    foreach(scandir($dir) as $file)
    {
        if($file == '.' || $file == '..')
            continue;

        if(preg_match($pattern, $file))
            $files[] = $file;
    }

    return $files;
}