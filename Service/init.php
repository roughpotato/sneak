<?php

return array(
    'getDirectories' => __DIR__.'/directory.php',
    'toUpperCamelCase' => __DIR__.'/string.php',
    'isValidUriPart' => __DIR__.'/uri.php',
    'getFunctionAttributes' => __DIR__.'/function.php'
);