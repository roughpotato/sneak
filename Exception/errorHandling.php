<?php

load('response');

function composeMessage($e)
{
    return sprintf(
        'Exception: %s, File: %s, Line: %s, Message: %s',
        get_class($e),
        $e->getFile(),
        $e->getLine(),
        $e->getMessage()
    );
}

function exceptionHandler(Throwable $e)
{
    $message = composeMessage($e);

    response($message, $e->getCode());

    error_log($message);

    echo $message;

    exit;
}