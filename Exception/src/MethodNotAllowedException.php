<?php

namespace Sneak\Exception;

use Exception;

class MethodNotAllowedException extends Exception
{
    public function __construct(string $msg = "Method Not Allowed")
    {
        parent::__construct($msg, 405);
    }
}