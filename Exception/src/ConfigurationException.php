<?php

namespace Sneak\Exception;

use Exception;

class ConfigurationException extends Exception
{
    public function __construct(string $msg = "Configuration Exception")
    {
        parent::__construct($msg, 500);
    }
}