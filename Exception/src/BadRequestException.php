<?php

namespace Sneak\Exception;

use Exception;

class BadRequestException extends Exception
{
    public function __construct(string $msg='Bad Request')
    {
        parent::__construct($msg, 400);
    }
}