<?php

namespace Sneak\Exception;

use Exception;

class RouteNotFoundException extends Exception
{
    public function __construct(array $route = [])
    {
        $msg = "The route you're trying to reach doesn't exist ";
        
        if(count($route))
        {
            $msg .=  'dir : ' . $route['dir'] . ', action : ' . $route['action'];
        }
        
        parent::__construct($msg, 404);
    }
}