<?php

namespace Sneak\Exception;

use Exception;

class GenericException extends Exception
{
    public function __construct(string $msg = "Generic Exception")
    {
        parent::__construct($msg, 500);
    }
}