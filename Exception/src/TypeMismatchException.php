<?php

namespace Sneak\Exception;

use Exception;

class TypeMismatchException extends Exception
{
    public function __construct(string $expected, $actual)
    {
        parent::__construct("Expected $expected, got ".gettype($actual), 500);
    }
}