<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'Env' . DIRECTORY_SEPARATOR . 'init.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'Autoloading' . DIRECTORY_SEPARATOR . 'classAutoloader.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'Autoloading' . DIRECTORY_SEPARATOR . 'functionRegisterer.php';

$subdirectories = array(
    'Exception',
    'Http',
    'Service',
    'Subject',
    'Logging',
    'Route',
);

register($subdirectories, __DIR__);

ini_set('display_errors', getenv(NGIN_ENV_VAR_PREFIX . 'DISPLAY_ERRORS', true));

load('exceptionHandler');

set_exception_handler('exceptionHandler');